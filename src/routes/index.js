import React, { Component } from "react"
import { BrowserRouter, Switch, Route } from "react-router-dom"
import HomeContainer from "../containers/home"
import LoginContainer from "../containers/login"

export default class App extends Component {
	render() {
		const isLoggedIn = localStorage.getItem("token")

		return (
			<BrowserRouter>
				<Switch>
					<Route exact path="/" component={() => <LoginContainer />} />
					{isLoggedIn && (
						<Route path="/home" component={() => <HomeContainer />} />
					)}
				</Switch>
			</BrowserRouter>
		)
	}
}
