export const PLACEHOLDER_URL = "https://via.placeholder.com/300"
export const REQUEST_HEADER = {
	headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
}
export const BASE_URI = "https://api.spotify.com/v1"
