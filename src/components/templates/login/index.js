import React, { Component } from "react"
import PropTypes from "prop-types"

import styles from "./login.scss"

export default class Login extends Component {
	render() {
		const { token, fetchLoginToken = () => {} } = this.props

		if (token) {
			localStorage.setItem("token", token)
			window.location.replace("/home")
		}

		return (
			<button className={styles.submitButton} onClick={fetchLoginToken}>
				Click To Login
			</button>
		)
	}
}

Login.propTypes = {
	token: PropTypes.string,
	fetchLoginToken: PropTypes.func,
}
