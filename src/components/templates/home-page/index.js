import React, { Component } from "react"
import PropTypes from "prop-types"

import styles from "./home-page.scss"

import Artist from "../../organisms/artist"
import Album from "../../organisms/album"

export default class HomePage extends Component {
	constructor(props) {
		super(props)

		this.state = {
			searchType: "artist",
			redirectToLogin: false,
			searchElement: "",
		}

		this.handleInputChange = this.handleInputChange.bind(this)
		this.searchForSpotifyData = this.searchForSpotifyData.bind(this)
	}

	searchForSpotifyData() {
		const { searchElement, searchType } = this.state

		if (searchElement === "") {
			return
		}

		this.props.fetchSpotifySearchData(searchElement, searchType)
	}

	handleInputChange(event) {
		const { name, value } = event.target

		this.setState({
			[name]: value,
		})
	}

	getArtistsData() {
		const {
			spotifySearchData = {},
			fetchAlbumsOfArtist,
			albumsOfArtists,
		} = this.props

		const artists = spotifySearchData.artists
		const artistItems = (artists || {}).items

		return (artistItems || []).map(element => (
			<Artist
				albumsOfArtists={albumsOfArtists}
				fetchAlbumsOfArtist={fetchAlbumsOfArtist}
				artistData={element}
			/>
		))
	}

	getAlbumsData() {
		const {
			spotifySearchData = {},
			fetchArtistofAlbum,
			artistOfAlbum,
		} = this.props

		const albums = spotifySearchData.albums
		const albumsItems = (albums || {}).items

		return (albumsItems || []).map(element => (
			<Album
				fetchArtistofAlbum={fetchArtistofAlbum}
				albumData={element}
				artistOfAlbum={artistOfAlbum}
			/>
		))
	}

	render() {
		const { spotifySearchData = {} } = this.props
		const hasArtistData = Object.keys(spotifySearchData)[0] === "artists"

		return (
			<>
				<div className={styles.searchBar}>
					<input
						placeholder="search.."
						className={styles.searchInput}
						name="searchElement"
						onChange={this.handleInputChange}
						type="text"
					/>
					<input
						value="artist"
						name="searchType"
						type="radio"
						defaultChecked
						className={styles.artistRadioButton}
						onChange={this.handleInputChange}
					/>
					Artist
					<input
						value="album"
						name="searchType"
						type="radio"
						className={styles.albumRadioButton}
						onChange={this.handleInputChange}
					/>
					Album
					<button
						className={styles.seearchButton}
						onClick={this.searchForSpotifyData}
					>
						Search
					</button>
				</div>
				<div>
					{hasArtistData ? this.getArtistsData() : this.getAlbumsData()}
				</div>
			</>
		)
	}
}

HomePage.propTypes = {
	spotifySearchData: PropTypes.object,
	fetchSpotifySearchData: PropTypes.func,
}
