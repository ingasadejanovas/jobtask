import React from "react"
import PropTypes from "prop-types"

export const Close = () => (
	<svg viewBox="0 0 23.71 23.71">
		<path
			fill="currentColor"
			d="M23.71.71L23 0 11.85 11.15.71 0 0 .71l11.15 11.14L0 23l.71.71 11.14-11.15L23 23.71l.71-.71-11.15-11.15L23.71.71z"
		/>
	</svg>
)

const icons = {
	close: [Close],
}

const Icon = ({ type }) => {
	const [Icon] = icons[type]

	return Icon && <Icon />
}

Icon.propTypes = {
	type: PropTypes.string,
}

export default Icon
