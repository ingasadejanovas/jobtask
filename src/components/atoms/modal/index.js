import React from "react"
import PropTypes from "prop-types"

import Icon from "../../atoms/icons"

import styles from "./modal.scss"

export const Modal = ({ children }) => (
	<div className={styles.root}>
		<div className={styles.children}>
			{children}
			<button className={styles.close}>
				<Icon type="close" />
			</button>
		</div>
	</div>
)

Modal.propTypes = {
	children: PropTypes.element,
}

export default Modal
