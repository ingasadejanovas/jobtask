import React, { Component } from "react"
import PropTypes from "prop-types"

import { PLACEHOLDER_URL } from "../../../constants"

export default class AlbumModalContent extends Component {
	componentDidMount() {
		const { artistId, fetchAlbumsOfArtist } = this.props

		fetchAlbumsOfArtist(artistId)
	}

	render() {
		const { albumsOfArtists } = this.props
		const albums = albumsOfArtists.items

		const placeholderUrl = PLACEHOLDER_URL

		return (albums || []).map(element => (
			<div key={element.id}>
				<h3>
					{element.name} ({element.release_date})
				</h3>
				<img
					src={
						element.images.length
							? (element.images[1] || []).url
							: placeholderUrl
					}
				/>
			</div>
		))
	}
}

AlbumModalContent.propTypes = {
	artistId: PropTypes.string,
	albumsOfArtists: PropTypes.object,
}
