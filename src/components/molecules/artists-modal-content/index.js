import React, { Component } from "react"
import PropTypes from "prop-types"

import { PLACEHOLDER_URL } from "../../../constants"

export default class ArtistsModalContent extends Component {
	componentDidMount() {
		const { artistId, fetchArtistofAlbum } = this.props

		fetchArtistofAlbum(artistId)
	}

	render() {
		const { artistOfAlbum } = this.props
		const placeholderUrl = PLACEHOLDER_URL

		return (
			<div key={artistOfAlbum.id}>
				<h3>{artistOfAlbum.name}</h3>
				<img
					height="300"
					width="300"
					src={
						(artistOfAlbum.images || []).length
							? (artistOfAlbum.images[1] || []).url
							: placeholderUrl
					}
				/>
			</div>
		)
	}
}

ArtistsModalContent.propTypes = {
	artistId: PropTypes.string,
	albumsOfArtists: PropTypes.object,
}
