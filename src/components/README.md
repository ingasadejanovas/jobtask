#Atomic Design

The concept of Atomic Design has been applied to divide up many of the components into groups to help facilitate understanding of their size and purpose within the application. The groups used are:
1)Atoms, the smallest individual components that fit together into,
for example, (reusable button, link, input, icons(svg -> ..), modal, etc.)
2)Molecules, which group atoms together into small, reusable components.
3)Organisms, which contain groups of molecules and/or atoms into a larger component which fit together into
4)Templates, which represent the different "screens"
