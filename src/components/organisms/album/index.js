import React, { Component } from "react"
import PropTypes from "prop-types"

import styles from "./album.scss"

import Modal from "../../atoms/modal"
import ArtistsModalContent from "../../molecules/artists-modal-content"

import { PLACEHOLDER_URL } from "../../../constants"

export default class Album extends Component {
	constructor(props) {
		super(props)

		this.state = { isArtistModalOpen: false }
		this.toggleAlbumModal = this.toggleAlbumModal.bind(this)
	}

	toggleAlbumModal() {
		this.setState(state => ({
			isArtistModalOpen: !state.isArtistModalOpen,
		}))
	}

	render() {
		const placeholderUrl = PLACEHOLDER_URL
		const { albumData, fetchArtistofAlbum, artistOfAlbum } = this.props
		const { isArtistModalOpen } = this.state
		const artists = albumData.artists
		const artistId = artists.map(element => element.id)

		return (
			<div
				onClick={this.toggleAlbumModal}
				className={styles.item}
				key={albumData.id}
			>
				<h3>
					{albumData.name} ({albumData.release_date})
				</h3>
				<img
					height="300"
					width="300"
					src={
						albumData.images.length
							? (albumData.images[1] || []).url
							: placeholderUrl
					}
				/>
				{isArtistModalOpen && (
					<Modal>
						<ArtistsModalContent
							artistOfAlbum={artistOfAlbum}
							fetchArtistofAlbum={fetchArtistofAlbum}
							artistId={artistId}
						/>
					</Modal>
				)}
			</div>
		)
	}
}

Album.propTypes = {
	albumData: PropTypes.object,
	artistOfAlbum: PropTypes.object,
	fetchArtistofAlbum: PropTypes.func,
}
