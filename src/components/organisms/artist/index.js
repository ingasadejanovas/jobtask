import React, { Component } from "react"
import PropTypes from "prop-types"
import styles from "./artist.scss"

import Modal from "../../atoms/modal"
import AlbumModalContent from "../../molecules/album-modal-content"

import { PLACEHOLDER_URL } from "../../../constants"

export default class Artist extends Component {
	constructor(props) {
		super(props)

		this.state = { isArtistModalOpen: false }
		this.toggleArtistModal = this.toggleArtistModal.bind(this)
	}

	toggleArtistModal() {
		this.setState(state => ({
			isArtistModalOpen: !state.isArtistModalOpen,
		}))
	}

	render() {
		const placeholderUrl = PLACEHOLDER_URL
		const { artistData, albumsOfArtists, fetchAlbumsOfArtist } = this.props
		const { isArtistModalOpen } = this.state

		return (
			<div
				onClick={this.toggleArtistModal}
				className={styles.item}
				key={artistData.id}
			>
				<h3>{artistData.name}</h3>
				<img
					height="300"
					width="300"
					src={
						artistData.images.length
							? (artistData.images[1] || []).url
							: placeholderUrl
					}
				/>
				{isArtistModalOpen && (
					<Modal>
						<AlbumModalContent
							fetchAlbumsOfArtist={fetchAlbumsOfArtist}
							albumsOfArtists={albumsOfArtists}
							artistId={artistData.id}
						/>
					</Modal>
				)}
			</div>
		)
	}
}

Artist.propTypes = {
	artistData: PropTypes.object,
	albumsOfArtists: PropTypes.object,
	fetchAlbumsOfArtist: PropTypes.func,
}
