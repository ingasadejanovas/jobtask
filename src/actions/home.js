import axios from "axios"
import { REQUEST_HEADER, BASE_URI } from "../constants"

export const FETCH_SPOTIFY_SEARCH_DATA = "FETCH_SPOTIFY_SEARCH_DATA"
export const FETCH_ARTIST_OF_ALBUM = "FETCH_ARTIST_OF_ALBUM"
export const FETCH_ALBUMS_OF_ARTIST = "FETCH_ALBUMS_OF_ARTIST"

export const fetchSpotifySearchData = spotifySearchData => {
	return {
		type: FETCH_SPOTIFY_SEARCH_DATA,
		spotifySearchData,
	}
}

export const fetchSpotifySearchDataAction = (searchElement, searchType) => {
	return dispatch => {
		return axios
			.get(
				`${BASE_URI}/search?query=${searchElement}&type=${searchType}`,
				REQUEST_HEADER,
			)
			.then(response => {
				dispatch(fetchSpotifySearchData(response.data))
			})
			.catch(error => {
				console.log(error) //or dispatch action that handles error behavior
			})
	}
}

export const fetchArtistofAlbum = artistOfAlbum => {
	return {
		type: FETCH_ARTIST_OF_ALBUM,
		artistOfAlbum,
	}
}

export const fetchArtistofAlbumAction = artistId => {
	return dispatch => {
		return axios
			.get(`${BASE_URI}/artists/${artistId}`, REQUEST_HEADER)
			.then(response => {
				dispatch(fetchArtistofAlbum(response.data))
			})
			.catch(error => {
				console.log(error) //or dispatch action that handles error behavior
			})
	}
}

export const fetchAlbumsOfArtist = albumsOfArtists => {
	return {
		type: FETCH_ALBUMS_OF_ARTIST,
		albumsOfArtists,
	}
}

export const fetchAlbumsOfArtistAction = artistId => {
	return dispatch => {
		return axios
			.get(`${BASE_URI}/artists/${artistId}/albums`, REQUEST_HEADER)
			.then(response => {
				dispatch(fetchAlbumsOfArtist(response.data))
			})
			.catch(error => {
				console.log(error) //or dispatch action that handles error behavior
			})
	}
}
