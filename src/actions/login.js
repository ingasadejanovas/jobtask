import axios from "axios"

export const FETCH_LOGIN_TOKEN = "FETCH_LOGIN_TOKEN"

export const fetchLoginToken = token => {
	return {
		type: FETCH_LOGIN_TOKEN,
		token,
	}
}

export const fetchLoginTokenAction = () => {
	return dispatch => {
		return axios
			.get("/api/spotify-credentials")
			.then(response => {
				dispatch(fetchLoginToken(response.data))
			})
			.catch(error => {
				console.log(error)
			})
	}
}
