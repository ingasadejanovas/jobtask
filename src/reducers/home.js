import {
	FETCH_SPOTIFY_SEARCH_DATA,
	FETCH_ARTIST_OF_ALBUM,
	FETCH_ALBUMS_OF_ARTIST,
} from "../actions/home"

export default function(state = {}, action) {
	switch (action.type) {
		case FETCH_SPOTIFY_SEARCH_DATA:
			return {
				...action.spotifySearchData,
			}
		case FETCH_ARTIST_OF_ALBUM:
			return {
				...state,
				...action.artistOfAlbum,
			}
		case FETCH_ALBUMS_OF_ARTIST:
			return {
				...state,
				...action.albumsOfArtists,
			}

		default:
			return state
	}
}
