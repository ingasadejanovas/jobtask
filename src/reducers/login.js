import { FETCH_LOGIN_TOKEN } from "../actions/login"

export default function(state = {}, action) {
	switch (action.type) {
		case FETCH_LOGIN_TOKEN:
			return {
				...state,
				...action.token,
			}
		default:
			return state
	}
}
