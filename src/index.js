import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"
import loggerMiddleware from "redux-logger"
import rootReducer from "./reducers"
import { applyMiddleware, createStore } from "redux"
import thunkMiddleware from "redux-thunk"
import App from "./routes"

import "./styles/main.scss"

const store = createStore(
	rootReducer,
	applyMiddleware(thunkMiddleware, loggerMiddleware),
)

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("app"),
)

if (module.hot) {
	module.hot.accept()
}
