## Containers

Containers provide a bridge between our Redux Actions/Reducers and our Atomic Design Templates, and are intentionally left empty of any presentational UI code. Instead, they pass Redux actions and Store data as props to their associated Template components, where they can be executed and accessed. This provides a separation of concerns, keeping the Redux implementation separate from the code used to display it.
