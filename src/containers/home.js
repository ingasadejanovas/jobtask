import React, { Component } from "react"
import { connect } from "react-redux"
import HomePage from "../components/templates/home-page"

import {
	fetchSpotifySearchDataAction,
	fetchArtistofAlbumAction,
	fetchAlbumsOfArtistAction,
} from "../actions/home"

export class HomeContainer extends Component {
	render() {
		const {
			spotifySearchData,
			artistOfAlbum,
			albumsOfArtists,
			fetchSpotifySearchData,
			fetchArtistofAlbum,
			fetchAlbumsOfArtist,
		} = this.props

		return (
			<HomePage
				spotifySearchData={spotifySearchData}
				fetchSpotifySearchData={fetchSpotifySearchData}
				fetchArtistofAlbum={fetchArtistofAlbum}
				artistOfAlbum={artistOfAlbum}
				albumsOfArtists={albumsOfArtists}
				fetchAlbumsOfArtist={fetchAlbumsOfArtist}
			/>
		)
	}
}

const mapStateToProps = state => {
	return {
		spotifySearchData: state.home,
		artistOfAlbum: state.home,
		albumsOfArtists: state.home,
	}
}

const mapDispatchToProps = dispatch => ({
	fetchSpotifySearchData: (searchElement, searchType) =>
		dispatch(fetchSpotifySearchDataAction(searchElement, searchType)),
	fetchArtistofAlbum: artistId => dispatch(fetchArtistofAlbumAction(artistId)),
	fetchAlbumsOfArtist: artistId =>
		dispatch(fetchAlbumsOfArtistAction(artistId)),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(HomeContainer)
