import React, { Component } from "react"
import { connect } from "react-redux"
import Login from "../components/templates/login"

import { fetchLoginTokenAction } from "../actions/login"

export class LoginContainer extends Component {
	render() {
		const { token, fetchLoginToken } = this.props

		return <Login token={token} fetchLoginToken={fetchLoginToken} />
	}
}

const mapStateToProps = state => {
	return {
		token: state.login.token,
	}
}

const mapDispatchToProps = dispatch => ({
	fetchLoginToken: () => dispatch(fetchLoginTokenAction()),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(LoginContainer)
