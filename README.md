#Home Task

Task is created with React, Redux, Webpack, Express.
Configuration (webpack and express) is created only for development mode

To start development run ---> "npm install", "npm run build" and then "npm start"

Go to http://localhost:3000/ or use your IP which is written in your terminal.

Project is using CSS modules , which means that classNames are local , not global , and css className output name is generated using file name and className , for example, we have file "home-page" and <div className={styles.searchBar} /> , className output will be "home-page\_\_searchBar".

Task at the moment is without pagination for spotify data (that's why we see only 20 records), and without error/loading behaviour
