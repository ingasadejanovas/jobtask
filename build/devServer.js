const path = require("path")
const express = require("express")
const webpack = require("webpack")
const ip = require("ip")
const config = require("./webpack.config.js")
const webpackHotMiddleware = require("webpack-hot-middleware")
const webpackDevMiddleware = require("webpack-dev-middleware")
let request = require("request")

const app = express()

const compiler = webpack(config)

app.get("/api/spotify-credentials", function(req, resp) {
	const authOptions = {
		url: "https://accounts.spotify.com/api/token",
		headers: {
			Authorization:
				"Basic  MDBiMjZjN2MwYmYwNDYzYmE0NWIyMzkwOGIzODAzNDM6MDgyMjdmN2Y2ZTZjNDNkZGIwNzQyM2Q5MjRlYTQ2NjQ=",
		},
		form: {
			grant_type: "client_credentials",
		},
		json: true,
	}

	request.post(authOptions, function(error, response, body) {
		if (!error && response.statusCode === 200) {
			resp.json({ token: body.access_token })
		}
	})
})

app.use(
	webpackDevMiddleware(compiler, {
		noInfo: true,
		publicPath: "/",
	}),
)

app.use(webpackHotMiddleware(compiler))

app.use(express.static("dist"))

app.get("*", (req, res) => {
	res.sendFile(path.join(__dirname, "../dist/index.html"))
})

const PORT = process.env.PORT || 3000

app.listen(PORT, "0.0.0.0", () => {
	console.log(
		`
		-- Ingas App --
		Listening: ${ip.address()}:${PORT}
		`.replace(/\t/g, ""),
	)
})
